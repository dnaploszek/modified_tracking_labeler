===================================
======= Skroty klawiszowe: ========
===================================

a - poprzednia klatka
d - następna klatka
s - start/stop filmu

q - poczatek zaznaczenia
w - koniec zaznaczenia
e - zaznacz w tyl
r - czysc zaznaczenie

0 lub ` - tracker: brak
1 - tracker: white hot
2 - tracker: dark hot
3 - tracker: template

ctrl+z - cofnij

===================================
===== Korzystanie z trackera: =====
===================================

Za pomoca listy rozwijanej "Tracker" mozna wybrac tryb recznego oznaczania celu (Brak) lub jeden z dostepnych trybow trackera. W trybie trackera, mozliwe jest zaznaczenie okna 
do inicjalizacji trackera, a nastepnie, przewijajac film w sposob ciagly w tyl lub przod
(bez klikania na pasek), automatyczne nanoszenie ramek za pomoca wybranego trackera.
Aby wykorzystac te ramki do zadania pozycji celu, nalezy stworzyc zaznaczenie zaczynajace
sie od na pierwszej klatce, gdzie tracker dziala prawidlowo i jego ramka nie styka sie z brzegiem okna i w trybie recznym oznaczyc na tej klatce cel. Nastepnie nalezy kliknac 
przycisk "Pozycja z trackera".

===================================
===== Objasnienia interfejsu: =====
===================================

POCZ - ustawia poczatek zaznaczenia na obecnej klatce

KON - ustawia koniec zaznaczenia na obecnej klatce

W TYL - zaznacza w tyl od obecnej klatki do pierwszej niepustej wlacznie

CZYSC - resetuje zaznaczenie

---

Nowy - nowy opis na podstawie wybranego filmu

Wczytaj - wczytaj istniejacy plik .yaml z opisem

Zapisz - zapisz opis w katalogu filmu pod nazwa:
[NAZWA FILMU BEZ ROZSZ.]-[INDEKS CELU].yaml (np. samolot-1.yaml)

---

Przyciski od "Interpolacja pozycji" do "Wyczysc tracker" dzialaja na obecnym zaznaczeniu,
a w przypadku jego braku, na obecnej klatce

Interpolacja pozycji / rozmiaru - dzialaja od pierwszej do ostatniej klatki zaznaczenia

Pozycja z trackera - odtwarza wzgledne polozenie celu i ramki trackera z pierwszej klatki
w zaznaczeniu na kazdej z pozostalych

Cofnij - dziala tylko dla operacji na klatkach, nie dla zmiany klatki czy parametrow
recznych (mozna cofnac do 20 razy)

===================================
===== Objasnienia parametrow: =====
===================================

Rodzaj - rodzaj celu: powierzny / naziemny

Widocznosc - ocena wyraźności celu w skali 1-5 (1-bardzo słaba, 5-bardzo dobra)

Szybka kamera - występowanie szybkich ruchów kamery

Wymijanie ruch. - bliskie wymijanie się celu z ruchomymi lub podobnymi do celu obiektami

Wymijanie stat. - wymijanie się celu z nieruchomymi obiektami lub wyraźnym tłem 
(np. drzewa, ziemia)

L. obiekt. - liczba obiektów na filmie podobnych do celu (włacznie z celem)

---

Ozn. wszystkie - oznaczono cel na wszystkich klatkach (czasami jest to niemozliwe)

Max v liniowa - maksymalna predkosc liniowa celu [pix/klatke]

Max a liniowe - maksymalne przyspieszenie liniowe celu [pix/klatke^2]

Med wxh - medianowe wymiary okna celu


