TARGET = dataset_labeler
TEMPLATE = app

# Config
QT        += gui
QT        += core
CONFIG    += console
CONFIG    += link_pkgconfig
CONFIG    -= app_bundle
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
INCLUDEPATH += include

# Libraries - LINUX
#unix {
#    LIBS += F:\opencv\opencv\sources\3rdparty\lib
#    PKGCONFIG += opencv
#    PKGCONFIG += yaml-cpp
#    LIBS      += -lboost_system -lboost_filesystem
#    LIBS      += -lpng -lstdc++
#}
win32 {
    INCLUDEPATH += "F:\opencv\opencv\build\include" \

    INCLUDEPATH += F:/yaml/yaml/include
    DEPENDPATH += F:/yaml/yaml/include
    CONFIG(debug,debug|release) {
        LIBS += -L"F:\opencv\opencv\build\x64\vc12\lib" \
            -lopencv_core2413d \
            -lopencv_highgui2413d \
            -lopencv_imgproc2413d \
            -lopencv_features2d2413d \
            -lopencv_gpu2413d \
            -lopencv_calib3d2413d \
            -lopencv_video2413d
        LIBS += -L"F:/yaml/yaml-build/debug/" -llibyaml-cppmdd
    }

    CONFIG(release,debug|release) {
        DEFINES += QT_NO_WARNING_OUTPUT QT_NO_DEBUG_OUTPUT
        LIBS += -L"F:\opencv\opencv\build\x64\vc12\lib" \
            -lopencv_core2413 \
            -lopencv_highgui2413 \
            -lopencv_imgproc2413 \
            -lopencv_features2d2413 \
            -lopencv_gpu2413 \
            -lopencv_calib3d2413 \
            -lopencv_video2413
        LIBS += -L"F:/yaml/yaml-build/release/" -llibyaml-cppmd
    }
}
# Headers
HEADERS += include/mainwindow.h
HEADERS += include/segmentation.h
HEADERS += include/videoclass.h
HEADERS += include/tracker.h
HEADERS += include/video_display.h
HEADERS += include/video_status.h
HEADERS += include/cv_image_widget.h
HEADERS += include/video_bar.h
HEADERS += include/sequencedialog.h

# Sources
SOURCES += src/mainwindow.cpp
SOURCES += src/segmentation.cpp
SOURCES += src/videoclass.cpp
SOURCES += src/tracker.cpp
SOURCES += src/video_status.cpp
SOURCES += src/cv_image_widget.cpp
SOURCES += src/video_bar.cpp
SOURCES += src/video_display.cpp
SOURCES += src/main.cpp
SOURCES += src/sequencedialog.cpp

# Forms
FORMS   += ui/mainwindow.ui
FORMS   += ui/sequencedialog.ui



