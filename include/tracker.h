#ifndef TRACKER_H
#define TRACKER_H

#include <iostream>
#include <opencv2/opencv.hpp>
using namespace std;
using namespace cv;

class Tracker
{
public:

    /** \brief Constructor */
    Tracker();

    /** \brief Set the current target */
    void setTarget( const Mat &image, const Rect& rect, int _mode );

    /** \brief Track the target in a new frame */
    Rect track( Mat &image );

private:

    /** \brief Template tracking margins */
    const int templ_mx, templ_my;

    /** \brief Current tracker mode */
    int mode;

    /** \brief Misc images used for processing */
    Mat prepr_img, templ, match_res, tmp;

    /** \brief Newest target rect */
    Rect curr_target;

    /** \brief Termination criteria set to 1 iteration */
    TermCriteria centroid_criteria;

    /** \brief Preprocess the input image */
    void preprocess( const Mat &input, Mat &output );
};

#endif
