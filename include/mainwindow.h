#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTimer>
#include <QKeyEvent>
#include <cv_image_widget.h>
#include <video_status.h>
#include <tracker.h>
#include <sequencedialog.h>
#include <videoclass.h>
#include <video_display.h>
#include <segmentation.h>

using namespace std;
using namespace cv;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:

    //==============================================================
    //======================= PUBLIC METHODS =======================
    //==============================================================

    /** \brief Constructor */
    explicit MainWindow( int argc, char** argv, QWidget *parent = 0 );

    /** \brief Destructor */
    ~MainWindow();

private slots:

    //=====================================================
    //======================= SLOTS =======================
    //=====================================================

    /** \brief Push button slots */
    // @{
    void on_btn_start_stop_clicked();
    void on_btn_next_pressed();
    void on_btn_next_released();
    void on_btn_prev_pressed();
    void on_btn_prev_released();
    void on_btn_pocz_zazn_clicked();
    void on_btn_koniec_zazn_clicked();
    void on_btn_tylne_zazn_clicked();
    void on_btn_czysc_zazn_clicked();
    //
    void on_btn_next_fast_pressed();
    void on_btn_next_fast_released();
    void on_btn_prev_fast_pressed();
    void on_btn_prev_fast_released();
    //
    void on_btn_nowy_clicked();
    void on_btn_nowy_2_clicked();
    void on_btn_zapisz_clicked();
    void on_btn_wczytaj_clicked();
    //
    void on_btn_interpolacja_pozycji_clicked();
    void on_btn_interpolacja_rozmiaru_clicked();
    void on_btn_wyczysc_cel_clicked();
    void on_btn_wyczysc_tracker_clicked();
    void on_btn_cofnij_clicked();
    //

    // }@

    /** \brief Tracking method set */
    void on_combo_tracker_currentIndexChanged( int index );

    /** \brief Timer callback for offline video captures */
    void onTimer();

    /** \brief Callback from video display event */
    void videoDisplayCallback( Label label );

    /** \brief Callback from video display event */
    void videoDisplay2Callback( Label label );

    /** \brief Callback from video display event */
    void videoDisplay3Callback( Label label );

    /** \brief Callback from video bar event */
    void videoBarCallback( int x );

    /** \brief Sequence window callback */
    void newSequencePath(QString path, QString name, QString path2, QString name2, QString path3, QString name3);

    void on_grabCutButton_clicked();

    void on_meanShiftButton_clicked();

    void on_cofnijSegButton_clicked();

    void on_watershedButton_clicked();

    void on_spin_indeks_celu_valueChanged(int arg1);

    void on_msSpatialWindowSpinBox_valueChanged(int arg1);

    void on_msColorWindowSpinBox_valueChanged(int arg1);

    void on_msMaxPyrSpinBox_valueChanged(int arg1);

    void on_gcIterSpinBox_valueChanged(int arg1);

    void on_addLabelFromSegmentationButton_clicked();

    void on_gcApplyButton_clicked();

    void on_msApplyButton_clicked();

private:

    //=================================================================
    //======================= PRIVATE VARIABLES =======================
    //=================================================================

    /** \brief Active label index */
    int labelIdx;

    /** \brief Video status history size (steps) */
    const int history_max_size;

    /** \brief Timer mode: 0 = idle, 1 = forward, -1 = backward */
    int timer_mode;

    /** \brief Ticks before timer takes action */
    int timer_countdown;

    /** \brief Next / prev buttons ticks of delay after pressing */
    int btn_delay;

    /** \brief Timer handling offline captures */
    QTimer *timer;

    /** \brief Objects wrapping up all video members */
    VideoClass firstVideo, secondVideo, mergeVideo;

    /** \brief Paths to the video files */
    QString firstVideoPath, secondVideoPath, mergeVideoPath;

    /** \brief User interface object */
    Ui::MainWindow *ui;

    /** \brief Sequence dialog handle */
    SequenceDialog *dialog;

    /** \brief Handle for segmentation object */
    Segmentation *seg;
   //===============================================================
   //======================= PRIVATE METHODS =======================
   //===============================================================

    /** \brief Display message in status bar area */
    void showMessage( string text );

    /** \brief Open video file */
    void openVideo( string path, string file_name, VideoClass &video, VideoDisplay *display);

    /** \brief Process the frame (form any origin) */
    void processNextFrame( VideoClass &video, VideoDisplay *display);

    /** \brief Add vstatus to history */
    void addHistory( VideoClass &video );

    void highlightSave( bool highlight );

    void processVideoDisplayCallback( Label label, VideoClass &video, VideoDisplay *display);

    void processVideoBarCallback ( int x, VideoClass &video, VideoDisplay *display);

    /** \brief Calculate global parameters */
    void calcGlobalParams( VideoClass &video );

    void writeToYaml( QString annot_path );
    void loadFromYaml( QString path );

    //@{
    /** \brief Handle keyboard events */
    void keyPressEvent( QKeyEvent *event );
    void keyReleaseEvent( QKeyEvent *event );
    //@}
};

#endif // MAINWINDOW_H
