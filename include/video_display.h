#ifndef VIDEO_DISPLAY_H
#define VIDEO_DISPLAY_H

#include <cv_image_widget.h>
#include <QMouseEvent>
#include <video_status.h>
#include <segmentation.h>

/** \brief Panel displaying the video and annotations */
class VideoDisplay : public CVImageWidget
{
    Q_OBJECT
public:

    /** \brief True if currently in tracking mode */
    bool tracking_mode;

    /** \brief Constructor */
    VideoDisplay( QWidget *parent );

    /** \brief Redraw label and last input */
    void draw(  );

    /** \brief Draw the label and tracker rects */
    void draw( const Mat &_input, const Label &_label );

    /** \brief Draw the label, previous label and tracker rects */
    void draw( const Mat &_input, const Label &_label, const Label&_prev_label );

    /** \brief Draw modified image without saving it */
    void drawModified( const Mat &_input );

    /** \brief Update the label and re-draw last input */
    void update( const Label &_label );

    /** \brief Set blank frame (optional red cross) */
    void drawBlankFrame( bool bad_frame=false );

    /** \brief Handles for mouse events due to multiple display usage */
    void handleMousePressEvent( QMouseEvent *event );
    void handleMouseMoveEvent( QMouseEvent *event );
    void handleMouseReleaseEvent( QMouseEvent *event );

    /** \brief Used to activate tools */
    void setManipulateOne(bool isManipulateOne) { manipulateOne = isManipulateOne; }
    void setMoveTool(bool isMoveTool_) { isMoveTool = isMoveTool_; }
    void setResizeTool(bool isResizeTool_) { isResizeTool = isResizeTool_; }

    /** \brief Used to set second display handle */
    void setSecondVideoDisplay( VideoDisplay *second ) { secondDisplay = second; }

    /** \brief Initialize grabCut segmentation */
    void segmentGrabCut();

    /** \brief Initialize meanShift segmentation */
    void segmentMeanShift();

    /** \brief Initialize watershed segmentation */
    void segmentWatershed();

    /** \brief Set allow manipulation flag
        \param value to set */
    void setAllowManipulation(bool value);

    /** \brief Sets the active label index
        \param newIndex to be set */
    void setLabelIndex(int newIndex) { labelIdx = newIndex; }

    /** \brief Sets the active label from segmentation rect*/
    void setLabelFromRect();

    void setSeg(Segmentation *value);

signals:

    /** \brief Emited on new video bar position */
    void vdisplayEvent( Label label );

protected:

    /** \brief Handle mouse events */
    void mousePressEvent( QMouseEvent *event );
    void mouseMoveEvent( QMouseEvent *event );
    void mouseReleaseEvent( QMouseEvent *event );

    /** \brief Handle resize events */
    void resizeEvent(QResizeEvent *event);

    /** \brief Methods used to manipulate labels*/
    void makeNewLabel(int x_, int y_);
    void moveLabel(int x_, int y_);
    void resizeLabel(int x_, int y_);

    /** \brief True if video manipulation is allowed */
    bool allowManipulation = true;

    /** \brief True when left btn is down */
    bool mouse_down;

    /** \brief Where the mous was pressed */
    Point down_pt;

    /** \brief Last input image */
    Mat input;

    /** \brief Label index */
    int labelIdx;

    /** \brief Current label */
    Label label;

    /** \brief Segmentation label rect */
    cv::Rect labelRect;

    /** \brief Previous label */
    Label prev_label;

    /** \brief Flag determining if previous label should be drawed*/
    bool draw_prev;

    /** \brief Flag determining if click should only affect one of the displays*/
    bool manipulateOne = false;

    /** \brief Flag determining if move label tool is used */
    bool isMoveTool = false;

    /** \brief Flag determining if resize label tool is used */
    bool isResizeTool = false;

    /** \brief Handle to second video display */
    VideoDisplay *secondDisplay;

    /** \brief Handle for segmentation object */
    Segmentation *seg;
};

#endif
