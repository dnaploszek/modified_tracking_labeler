#ifndef SEQUENCEDIALOG_H
#define SEQUENCEDIALOG_H

#include <QDialog>
#include <QRunnable>
#include <QFileDialog>

namespace Ui {
class SequenceDialog;
}

class GenerateSequenceThread : public QObject, public QRunnable
{
    Q_OBJECT
private:
    QStringList selectedPhotoPaths;
    QString sequenceDir, sequenceName;
    int getDigits(int number)
    {
        int digits = 0;
        while (number) {
            number /= 10;
            digits++;
        }
        return digits;
    }

public:
    GenerateSequenceThread(QStringList _selectedPhotoPaths, QString _sequenceDir, QString _sequenceName) :
        selectedPhotoPaths(_selectedPhotoPaths), sequenceDir(_sequenceDir), sequenceName(_sequenceName) {}

    void run()
    {
        int numberOfFiles = selectedPhotoPaths.size();
        int globalDigits = getDigits(numberOfFiles);
        emit setProgress(0, numberOfFiles);
        for(int i = 0 ; i < numberOfFiles; i++)
        {
            QString number = "";
            int iDigits = getDigits(i+1);
            for(int j = 0 ; j < globalDigits - iDigits ; j++)
            {
                number += "0";
            }
            number += QString::number(i+1);
            QFile::copy(selectedPhotoPaths.at(i), sequenceDir + "/" + sequenceName +number + ".bmp");
            emit setProgress(i+1, numberOfFiles);
        }

        emit setProgress(numberOfFiles, numberOfFiles);
        emit finished();
    }

signals:
    void finished();
    void setProgress(int file, int files);
};

class SequenceDialog : public QDialog
{
    Q_OBJECT

public:
    explicit SequenceDialog(QWidget *parent = 0);
    ~SequenceDialog();

private slots:
    void on_photosButton_clicked();

    void on_sequencNameEdit_textChanged(const QString &arg1);

    void on_sequenceDirButton_clicked();

    void on_generateSequenceButton_clicked();

    void on_getSequenceButton_clicked();

    void on_buttonBox_accepted();

    void on_sequenceDirEdit_textChanged(const QString &arg1);

    void on_generateSequenceThread_progress(int file, int files);

    void on_generateSequenceThread_finished();

    void on_getSequenceButton_3_clicked();

    void on_getSequenceButton_4_clicked();

signals:
    void sequencePathReady(QString path, QString name, QString path2, QString name2, QString path3, QString name3);

private:
    Ui::SequenceDialog *ui;

    /** \brief Paths to files which will form a sequence */
    QStringList selectedPhotoPaths;

    /** \brief Sequence names */
    QString sequenceName = "";
    QString sequenceName_2 = "";
    QString sequenceName_3 = "";


    /** \brief Sequence directory */
    QString sequenceDir;

    /** \brief Sequence directory + first file name */
    QString sequencePath = "";
    QString sequencePath_2 = "";
    QString sequencePath_3 = "";

    /** \brief Thread used to generate sequence */
    GenerateSequenceThread *thread;

    bool pathValid = false;
    bool pathValid_2 = false;
    bool pathValid_3 = false;
    bool photosOk = false;
    bool nameOk = false;
    bool dirOk = false;

    void checkIfReady();
    void checkFileName();
};

#endif // SEQUENCEDIALOG_H
