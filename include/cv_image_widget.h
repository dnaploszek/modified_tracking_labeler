#ifndef CV_IMAGE_WIDGET_H
#define CV_IMAGE_WIDGET_H

#include <iostream>
#include <QWidget>
#include <QImage>
#include <QPainter>
#include <opencv2/opencv.hpp>

using namespace std;
using namespace cv;

class CVImageWidget : public QWidget
{
    Q_OBJECT
public:
    explicit CVImageWidget( QWidget *parent = 0 );
    QSize sizeHint() const;
    QSize minimumSizeHint() const;

public slots:
    void showImage( const cv::Mat& image, bool fixed_shape=true );

protected:
    void paintEvent( QPaintEvent* /*event*/ );
    cv::Point widget2ImageCoords( const Point &widget_pt );

    QImage _qimage;
    cv::Mat _input_image, _tmp;
    int x_margin, y_margin;
    double x_factor, y_factor;
};

#endif
