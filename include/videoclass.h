#ifndef VIDEOCLASS_H
#define VIDEOCLASS_H

#include <cv_image_widget.h>
#include <video_status.h>
#include <tracker.h>

class VideoClass
{
private:
    /** \brief True if we are next to a tracked frame */
    bool trackingContinuity;

    /** \brief File name of the labeled video */
    string videoName;

    /** \brief Directory containing the video file */
    string videoDir;

public:
    VideoClass();

    /** \brief Video capture object */
    VideoCapture capture;

    /** \brief Holds frame-related data */
    VideoStatus vstatus;

    /** \brief Previous vstatus structures for undoing */
    vector<VideoStatus> vstatus_history;

    /** \brief Frame-by-frame object tracker */
    Tracker tracker;

    /** \brief Last read frame */
    Mat grayImg;

    bool            getTrackingContinuity() const;
    string          getVideoName() const;
    string          getVideoDir() const;

    void setTrackingContinuity(bool value);
    void setVideoName(const string &value);
    void setVideoDir(const string &value);
};

#endif // VIDEOCLASS_H
