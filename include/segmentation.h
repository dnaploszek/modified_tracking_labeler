#ifndef SEGMENTATION_H
#define SEGMENTATION_H

#include <opencv2/opencv.hpp>
#include <video_status.h>

enum ValueCode {
    GC_ITER,
    MS_SPATIAL,
    MS_COLOR,
    MS_MAXPYR
};

class Segmentation
{
public:
    Segmentation();

    void setSegmentationOption(ValueCode code, int val);
    cv::Mat grabCutSegment(cv::Mat img, Label label);
    cv::Mat meanShiftSegment(cv::Mat img);
    cv::Mat watershedSegment(cv::Mat img);
    cv::Rect getGrabCutRect();
private:
    // Grab cut
    int gcIterations;
    cv::Rect gcBoundingRect;

    // Mean shift
    int spatialWindow;
    int colorWindow;
    int maxPyr;

};

#endif // SEGMENTATION_H
