#ifndef VIDEO_BAR_H
#define VIDEO_BAR_H

#include <cv_image_widget.h>
#include <QMouseEvent>
#include <video_status.h>

/** \brief Bar for controling the video status */
class VideoBar : public CVImageWidget
{
    Q_OBJECT
public:

    /** \brief Constructor */
    explicit VideoBar( QWidget *parent = 0 );

    /** \brief Draw the bar status */
    void draw( const VideoStatus &vstatus );

    /** \brief Set all black */
    void clear();

    /** \brief Sets the active label index
        \param newIndex to be set */
    void setLabelIndex(int newIndex){ label_index = newIndex; }

signals:

    /** \brief Emited on new video bar position */
    void vbarEvent( int x );

protected:

    //@{
    /** \brief Handle mouse events */
    void mousePressEvent( QMouseEvent *event );
    void mouseMoveEvent( QMouseEvent *event );
    void mouseReleaseEvent( QMouseEvent *event );
    //@}

    /** \brief True when left btn is down */
    bool mouse_down;

    /** \brief Index of the label */
    int label_index;

};

#endif
