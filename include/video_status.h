#ifndef VIDEO_STATUS_H
#define VIDEO_STATUS_H

#include <iostream>
#include <opencv2/opencv.hpp>

using namespace std;
using namespace cv;

/** \brief Single frame data */
struct Label
{
    int index;
    bool active, tracked;
    int x, y, w, h;
    Label();
    Rect getTargetRect() const;
    void setTargetRect( const Rect &target );
    Rect getActiveRect() const;
};

/** \brief Object holding all frame-related data */
class VideoStatus
{
public:

    //@{
    /** \brief Global video parameters */
    float max_v, max_a;
    int med_w, med_h;
    bool all_active;
    //@}

    /** \brief Multiple labels corresponding to each frame */
    vector<vector<Label>> labels;

    /** \brief Current frame index */
    int curr;

    /** \brief Constructor */
    VideoStatus();

    /** \brief Initialize with empty frames */
    void init( int num_frames );

    /** \brief Return reference to current frame's label */
    Label& currLabel();

    /** \brief Return reference to previous frame's label, if exists */
    Label& prevLabel();

    /** \brief Return frame count */
    int size() const;

    /** \brief Remove all frames (resize to 0) */
    void reset();

    /** \brief Clear target labels in the selection */
    void clearTarget();

    /** \brief Clear tracker in the selection */
    void clearTracker();

    /** \brief Interpolate position from first to last selection frame */
    void interpolatePosition();

    /** \brief Interpolate size from first to last selection frame */
    void interpolateSize();

    /** \brief Reproduce first frame's tracker-label relative position */
    void labelsFromTracker();

    /** \brief Calculate the global parameters */
    void calcGlobalParams();

    /** \brief Sets the active label index
        \param newIndex to be set */
    void setLabelIndex(int newIndex) { label_index = newIndex; }

    //@{
    /** \brief Get and set selection */
    int getSelStart() const;
    int getSelEnd() const;
    void setSelStart();
    void setSelEnd();
    void selectBehind();
    void clearSelection();
    //@}

private:

    /** \brief Selection start and end frames (inclusive) */
    int sel_start, sel_end;

    /** \brief Default label that can be referenced */
    Label default_label;

    /** \brief Index of the label */
    int label_index;
};

#endif
