#include <include/mainwindow.h>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <QFileDialog>
#include <yaml-cpp/yaml.h>
#include <video_bar.h>
#include <segmentation.h>
#include "ui_mainwindow.h"

#define CAPTURE_POS() video.capture.get(CV_CAP_PROP_POS_FRAMES)
#define CAPTURE_SIZE() video.capture.get(CV_CAP_PROP_FRAME_COUNT)


//=====================================================
//======================= UTILS =======================
//=====================================================

bool fileExists( QString path ){
    QFileInfo check_file(path);
    // check if file exists and if yes: Is it really a file and no directory?
    if( check_file.exists() && check_file.isFile() )
        return true;
    else
        return false;
}

void setComboByText( QComboBox *combo, string text )
{
    for( int i=0; i<combo->count(); i++ ){
        if( combo->itemText(i) == QString::fromStdString(text) ){
            combo->setCurrentIndex(i);
            break;
        }
    }
}

template<class T>
void yamlEmit( YAML::Emitter &emitter, string key, T value )
{
    emitter << YAML::Key << key << YAML::Value << value;
}

//===============================================================================
//======================= GLOBAL, CONSTRUCTOR, DESTRUCTOR =======================
//===============================================================================

MainWindow::MainWindow( int argc, char** argv, QWidget *parent ) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    timer_mode(0),
    timer_countdown(0),
    btn_delay(0),
    history_max_size(20),
    labelIdx(0)
{
    ui->setupUi(this);
    ui->segmentationOptionsWidget->setVisible(false);

    seg = new Segmentation();
    // Display instructions
    showMessage( "Wczytaj lub utwórz nowy opis filmu" );

    // Connect signals / slots
    connect( ui->video_bar, SIGNAL(vbarEvent(int)),
             this, SLOT(videoBarCallback(int)) );
    connect( ui->video_display, SIGNAL(vdisplayEvent(Label)),
             this, SLOT(videoDisplayCallback(Label)) );
    connect( ui->video_display_2, SIGNAL(vdisplayEvent(Label)),
             this, SLOT(videoDisplay2Callback(Label)) );
    connect( ui->video_display_3, SIGNAL(vdisplayEvent(Label)),
             this, SLOT(videoDisplay3Callback(Label)) );
    ui->video_display->setSecondVideoDisplay(ui->video_display_2);
    ui->video_display_2->setSecondVideoDisplay(ui->video_display);
    ui->video_display->setSeg(seg);
    ui->video_display_2->setSeg(seg);
    ui->video_display_3->setAllowManipulation(false);

    // Set timer
    int frame_duration = 20; // ms
    int btn_time_delay = 300; // ms
    btn_delay = btn_time_delay / frame_duration;
    timer = new QTimer(this);
    timer->setInterval(frame_duration);
    connect(timer, SIGNAL(timeout()), this, SLOT(onTimer()));
    timer->start();
}

MainWindow::~MainWindow()
{
    delete timer;
    delete ui;
}



//============================================================
//======================= MISC METHODS =======================
//============================================================

void MainWindow::showMessage( string text )
{
    ui->status_label->setText( QString::fromStdString(text) );
}

void MainWindow::openVideo( string path, string file_name, VideoClass &video, VideoDisplay *display)
{
    video.setTrackingContinuity(false);
    video.vstatus_history.clear();

    QFileInfo fi(QString::fromStdString(path));
    string vid_name;
    string dir_path = fi.dir().absolutePath().toStdString();

    if(file_name == "")
        vid_name = fi.fileName().toStdString();
    else
        vid_name = file_name;
    video.capture.open( path );

    if( video.capture.isOpened() ){
        video.setVideoDir( dir_path );
        video.setVideoName( vid_name );
        timer_mode = 0;
        video.vstatus.init( CAPTURE_SIZE()-1 );
        processNextFrame( video, display);
    }else{
        showMessage( "Problem z otwarciem filmu" );
        video.vstatus.reset();
        display->drawBlankFrame(true);
        ui->video_bar->draw( video.vstatus );
        video.setVideoDir( "" );
        video.setVideoName( "" );
    }
    calcGlobalParams(video);
}

void MainWindow::onTimer()
{
    if( timer_mode == 0 || firstVideo.vstatus.size() == 0 || secondVideo.vstatus.size() == 0 ){ // Do nothing
        return;
    }else if( timer_countdown > 0 ){ // Reduce countdown
        --timer_countdown;
        // This allows one frame immediately after pressing
        if( timer_countdown < btn_delay-1 ) return;
    }

    // Set capture to prev frame if mode == -1
    if( timer_mode != 0 && timer_mode != 1 ){
        int frame_idx = firstVideo.capture.get(CV_CAP_PROP_POS_FRAMES);
        int new_frame_idx = frame_idx + timer_mode - 1;
        if( new_frame_idx < 0 )
            new_frame_idx = 0;
        if(new_frame_idx >= firstVideo.vstatus.size()-1)
            new_frame_idx = firstVideo.vstatus.size()-2;

        else
        {
            firstVideo.capture.set( CV_CAP_PROP_POS_FRAMES, new_frame_idx );
            secondVideo.capture.set( CV_CAP_PROP_POS_FRAMES, new_frame_idx );
            mergeVideo.capture.set( CV_CAP_PROP_POS_FRAMES, new_frame_idx );
        }
    }

    // Process next frame
    processNextFrame(firstVideo, ui->video_display);
    processNextFrame(secondVideo, ui->video_display_2);
    processNextFrame(mergeVideo, ui->video_display_3);
}

void MainWindow::processNextFrame( VideoClass &video, VideoDisplay *display)
{
    bool tracking_continuity = video.getTrackingContinuity();

    // Don't advance past the last label
    if( CAPTURE_POS() == video.vstatus.size() ) return;

    // Get next frame and update vbar
    Mat bgr_img;
    video.capture >> bgr_img;
    video.vstatus.curr = CAPTURE_POS()-1;
    ui->video_bar->draw( video.vstatus );

    // Handle bad frames
    if( bgr_img.empty() ){
        timer_mode = 0;
        display->drawBlankFrame(true);
        ui->video_bar->draw( video.vstatus );
        cout << "BAD FRAME: " << video.vstatus.curr << endl;
        return;
    }

    // Convert to gray and back
    cvtColor( bgr_img, video.grayImg, CV_BGR2GRAY );
    cvtColor( video.grayImg, bgr_img, CV_GRAY2BGR );

    // Tracker
    int tracker_mode = ui->combo_tracker->currentIndex();
    if( tracker_mode && !tracking_continuity && video.vstatus.currLabel().tracked ){
        video.tracker.setTarget( video.grayImg, video.vstatus.currLabel().getTargetRect(), tracker_mode );
        video.setTrackingContinuity( true );
    }else if( tracking_continuity ){
        Rect target = video.tracker.track( video.grayImg );
        video.vstatus.currLabel().setTargetRect( target );
    }

    // Display image
    if(ui->checkbox_show_prev->isChecked()){
        display->draw( bgr_img, video.vstatus.currLabel(), video.vstatus.prevLabel() );
    }
    else
        display->draw( bgr_img, video.vstatus.currLabel() );
}

void MainWindow::addHistory( VideoClass &video )
{
    if( video.vstatus_history.size() > history_max_size )
        video.vstatus_history.erase( video.vstatus_history.begin() );
    video.vstatus_history.push_back( video.vstatus );
    highlightSave(true);
}

void MainWindow::highlightSave( bool highlight )
{
    if( highlight )
        ui->btn_zapisz->setStyleSheet("* { background-color: rgb(255,0,0) }");
    else
        ui->btn_zapisz->setStyleSheet("");
}

void MainWindow::processVideoDisplayCallback( Label label, VideoClass &video, VideoDisplay *display )
{
    if( video.vstatus.size() == 0 ) return;
    addHistory(video);
    video.vstatus.currLabel() = label;
    calcGlobalParams(video);
    ui->video_bar->draw( video.vstatus );

    // Update tracker
    if( ui->combo_tracker->currentIndex() && label.tracked ){
        // Set target
        video.tracker.setTarget( video.grayImg, label.getTargetRect(),
                           ui->combo_tracker->currentIndex() );
        video.setTrackingContinuity(true);

        // Track the current frame to stabilize window position
        Rect target = video.tracker.track( video.grayImg );
        video.vstatus.currLabel().setTargetRect( target );
        display->update( video.vstatus.currLabel() );
    }
}

void MainWindow::processVideoBarCallback(int x, VideoClass &video, VideoDisplay *display)
{
    if( video.vstatus.size() == 0 ) return;
    timer_mode = 0;

    // Get the frame index
    int vbar_w = ui->video_bar->geometry().width();
    double frame_w = double(vbar_w) / video.vstatus.size();
    int frame_idx = x / frame_w;
    if( frame_idx < 0 ) frame_idx = 0;
    else if( frame_idx > video.vstatus.size()-1) frame_idx = video.vstatus.size()-1;

    // Interrupt tracking continuity
    video.setTrackingContinuity(false);

    // Get and process the frame
    video.capture.set( CV_CAP_PROP_POS_FRAMES, frame_idx );
    processNextFrame(video, display);
}

//================================================================
//======================= WIDGET CALLBACKS =======================
//================================================================

void MainWindow::videoDisplayCallback( Label label )
{
    processVideoDisplayCallback(label, firstVideo, ui->video_display);
}

void MainWindow::videoDisplay2Callback( Label label )
{
    processVideoDisplayCallback(label, secondVideo, ui->video_display_2);
}

void MainWindow::videoDisplay3Callback( Label label )
{
    processVideoDisplayCallback(label, mergeVideo, ui->video_display_3);
}

void MainWindow::videoBarCallback( int x )
{
    processVideoBarCallback(x, firstVideo, ui->video_display);
    processVideoBarCallback(x, secondVideo, ui->video_display_2);
    processVideoBarCallback(x, mergeVideo, ui->video_display_3);
}

void MainWindow::newSequencePath(QString path, QString name, QString path2, QString name2, QString path3, QString name3)
{
    if( path != "")
    {
        firstVideoPath = path;
        openVideo( path.toStdString(), name.toStdString(), firstVideo, ui->video_display);
    }
    if( path2 != "")
    {
        secondVideoPath = path2;
        openVideo( path2.toStdString(), name2.toStdString(), secondVideo, ui->video_display_2);
    }
    if( path3 != "")
    {
        mergeVideoPath = path2;
        openVideo( path3.toStdString(), name3.toStdString(), mergeVideo, ui->video_display_3);
    }
}

//===============================================================
//======================= INTERFACE SLOTS =======================
//===============================================================

void MainWindow::on_grabCutButton_clicked()
{
    ui->segmentationOptionsWidget->setVisible(true);
    ui->segmentationOptionsWidget->setCurrentIndex(1);
}

void MainWindow::on_gcApplyButton_clicked()
{
    ui->video_display->segmentGrabCut();
    ui->video_display_2->segmentGrabCut();
}

void MainWindow::on_meanShiftButton_clicked()
{
    ui->segmentationOptionsWidget->setVisible(true);
    ui->segmentationOptionsWidget->setCurrentIndex(0);
}

void MainWindow::on_msApplyButton_clicked()
{
    ui->video_display->segmentMeanShift();
    ui->video_display_2->segmentMeanShift();
}

void MainWindow::on_watershedButton_clicked()
{
    ui->segmentationOptionsWidget->setVisible(true);
    ui->segmentationOptionsWidget->setCurrentIndex(2);
    ui->video_display->segmentWatershed();
    ui->video_display_2->segmentWatershed();
}

void MainWindow::on_cofnijSegButton_clicked()
{
    ui->segmentationOptionsWidget->setVisible(false);
    ui->video_display->draw();
    ui->video_display_2->draw();
}

// Segmentation options
void MainWindow::on_gcIterSpinBox_valueChanged(int arg1)
{
    seg->setSegmentationOption(ValueCode::GC_ITER, arg1);
}

void MainWindow::on_msSpatialWindowSpinBox_valueChanged(int arg1)
{
    seg->setSegmentationOption(ValueCode::MS_SPATIAL, arg1);
}

void MainWindow::on_msColorWindowSpinBox_valueChanged(int arg1)
{
    seg->setSegmentationOption(ValueCode::MS_COLOR, arg1);
}

void MainWindow::on_msMaxPyrSpinBox_valueChanged(int arg1)
{
    seg->setSegmentationOption(ValueCode::MS_MAXPYR, arg1);
}

void MainWindow::on_addLabelFromSegmentationButton_clicked()
{
    ui->segmentationOptionsWidget->setVisible(false);
    ui->video_display->setLabelFromRect();
    ui->video_display_2->setLabelFromRect();
    ui->video_display->draw();
    ui->video_display_2->draw();

}

void MainWindow::on_btn_start_stop_clicked()
{
    if( timer_mode != 0 ){
        timer_mode = 0;
    }else{
        timer_mode = 1;
        timer_countdown = 0;
    }
}

void MainWindow::on_btn_next_pressed()
{
    timer_mode = 1;
    timer_countdown = btn_delay;
}

void MainWindow::on_btn_next_released()
{
    timer_mode = 0;
}

void MainWindow::on_btn_prev_pressed()
{
    timer_mode = -1;
    timer_countdown = btn_delay;
}

void MainWindow::on_btn_prev_released()
{
    timer_mode = 0;
}

void MainWindow::on_btn_next_fast_pressed()
{
    timer_mode = 10;
    timer_countdown = btn_delay;
}

void MainWindow::on_btn_next_fast_released()
{
    timer_mode = 0;
}

void MainWindow::on_btn_prev_fast_pressed()
{
    timer_mode = -10;
    timer_countdown = btn_delay;
}

void MainWindow::on_btn_prev_fast_released()
{
    timer_mode = 0;
}

void MainWindow::on_btn_pocz_zazn_clicked()
{
    addHistory(firstVideo);
    addHistory(secondVideo);
    firstVideo.vstatus.setSelStart();
    secondVideo.vstatus.setSelStart();
    ui->video_bar->draw( firstVideo.vstatus );
}

void MainWindow::on_btn_koniec_zazn_clicked()
{
    addHistory(firstVideo);
    addHistory(secondVideo);
    firstVideo.vstatus.setSelEnd();
    secondVideo.vstatus.setSelEnd();
    ui->video_bar->draw( firstVideo.vstatus );
}

void MainWindow::on_btn_tylne_zazn_clicked()
{
    firstVideo.vstatus.selectBehind();
    secondVideo.vstatus.selectBehind();
    firstVideo.vstatus.setSelEnd();
    secondVideo.vstatus.setSelEnd();
    ui->video_bar->draw( firstVideo.vstatus );
}

void MainWindow::on_btn_czysc_zazn_clicked()
{
    addHistory(firstVideo);
    addHistory(secondVideo);
    firstVideo.vstatus.clearSelection();
    secondVideo.vstatus.clearSelection();
    ui->video_bar->draw( firstVideo.vstatus );
}

void MainWindow::on_btn_nowy_clicked()
{
    // Select file dialog
    QString in_path = QFileDialog::getOpenFileName(
                0, "Wybierz pierwszy plik z filmem",
                QString::fromStdString(firstVideo.getVideoDir()),
                "Film (*.avi *.wmv)" );
    if( in_path == "" ) return;

    firstVideoPath = in_path;
    openVideo( in_path.toStdString(), "", firstVideo, ui->video_display);

    // Select file dialog
    in_path = QFileDialog::getOpenFileName(
                0, "Wybierz drugi plik z filmem",
                QString::fromStdString(secondVideo.getVideoDir()),
                "Film (*.avi *.wmv)" );
    if( in_path == "" ) return;

    secondVideoPath = in_path;
    openVideo( in_path.toStdString(), "", secondVideo, ui->video_display_2 );

    // Select file dialog
    in_path = QFileDialog::getOpenFileName(
                0, "Wybierz trzeci plik z filmem (dopasowanie - opcjonalne)",
                QString::fromStdString(secondVideo.getVideoDir()),
                "Film (*.avi *.wmv)" );
    if( in_path == "" ) return;

    mergeVideoPath = in_path;
    openVideo( in_path.toStdString(), "", mergeVideo, ui->video_display_3 );
}

void MainWindow::on_btn_nowy_2_clicked()
{
    dialog = new SequenceDialog();
    QObject::connect(dialog, SIGNAL(sequencePathReady(QString,QString,QString,QString,QString,QString)), this, SLOT(newSequencePath(QString,QString,QString,QString,QString,QString)));
    dialog->show();
}

void MainWindow::on_btn_zapisz_clicked()
{
    // Get annotation path
    QString filename = QFileDialog::getSaveFileName(
                0, "Podaj nazwę pliku do zapisu",
                QString::fromStdString(firstVideo.getVideoDir()) );
    if( filename == "" ) return;
    QFileInfo fi( filename );
    QString dir_path = fi.dir().absolutePath();
    QString base_name = fi.baseName();
    QString base_path = dir_path + "/" + base_name;
    int target_idx = ui->spin_indeks_celu->value();

    QString annot_path = base_path + "-" + QString::number(target_idx) +".yaml";
    writeToYaml(annot_path);
}

void MainWindow::on_btn_wczytaj_clicked()
{
    // Select file dialog
    QString in_path = QFileDialog::getOpenFileName(
                0, "Wybierz plik projektu",
                QString::fromStdString(secondVideo.getVideoDir()),
                "Film (*.yaml *.yml)" );
    if( in_path == "" ) return;

    loadFromYaml( in_path);
}

void MainWindow::on_btn_interpolacja_pozycji_clicked()
{
    addHistory(firstVideo);
    addHistory(secondVideo);
    firstVideo.vstatus.interpolatePosition();
    secondVideo.vstatus.interpolatePosition();
    calcGlobalParams(firstVideo);
    calcGlobalParams(secondVideo);
    ui->video_bar->draw( firstVideo.vstatus );
    ui->video_display->update( firstVideo.vstatus.currLabel() );
    ui->video_display_2->update( secondVideo.vstatus.currLabel() );
}

void MainWindow::on_btn_interpolacja_rozmiaru_clicked()
{
    addHistory(firstVideo);
    addHistory(secondVideo);
    firstVideo.vstatus.interpolateSize();
    secondVideo.vstatus.interpolateSize();
    calcGlobalParams(firstVideo);
    calcGlobalParams(secondVideo);
    ui->video_bar->draw( firstVideo.vstatus );
    ui->video_display->update( firstVideo.vstatus.currLabel() );
    ui->video_display_2->update( secondVideo.vstatus.currLabel() );

}

void MainWindow::on_btn_wyczysc_cel_clicked()
{
    addHistory(firstVideo);
    addHistory(secondVideo);
    firstVideo.vstatus.clearTarget();
    secondVideo.vstatus.clearTarget();
    calcGlobalParams(firstVideo);
    calcGlobalParams(secondVideo);
    ui->video_bar->draw( firstVideo.vstatus );
    ui->video_display->update( firstVideo.vstatus.currLabel() );
    ui->video_display_2->update( secondVideo.vstatus.currLabel() );
}

void MainWindow::on_btn_wyczysc_tracker_clicked()
{
    addHistory(firstVideo);
    addHistory(secondVideo);
    firstVideo.vstatus.clearTracker();
    secondVideo.vstatus.clearTracker();
    ui->video_display->update( firstVideo.vstatus.currLabel() );
    ui->video_display_2->update( secondVideo.vstatus.currLabel() );
}

void MainWindow::on_btn_cofnij_clicked()
{
    if( firstVideo.vstatus_history.size() == 0 ) return;
    firstVideo.vstatus = firstVideo.vstatus_history.back();
    firstVideo.vstatus_history.pop_back();
    calcGlobalParams(firstVideo);
    firstVideo.capture.set( CV_CAP_PROP_POS_FRAMES, firstVideo.vstatus.curr );
    processNextFrame(firstVideo, ui->video_display);
    highlightSave(true);

    if( secondVideo.vstatus_history.size() == 0 ) return;
    secondVideo.vstatus = secondVideo.vstatus_history.back();
    secondVideo.vstatus_history.pop_back();
    calcGlobalParams(secondVideo);
    secondVideo.capture.set( CV_CAP_PROP_POS_FRAMES, secondVideo.vstatus.curr );
    processNextFrame(secondVideo, ui->video_display_2);
    highlightSave(true);
}

//===============================================================
//======================= KEYBOARD EVENTS =======================
//===============================================================

void MainWindow::keyPressEvent( QKeyEvent *event )
{
    if( event->isAutoRepeat() ) return;
    else if( event->key() == Qt::Key_D )
        on_btn_next_pressed();
    else if( event->key() == Qt::Key_A )
        on_btn_prev_pressed();
    else if( event->key() == Qt::Key_S )
        on_btn_start_stop_clicked();
    else if( event->key() == Qt::Key_Q )
        on_btn_pocz_zazn_clicked();
    else if( event->key() == Qt::Key_W )
        on_btn_koniec_zazn_clicked();
    else if( event->key() == Qt::Key_E )
        on_btn_tylne_zazn_clicked();
    else if( event->key() == Qt::Key_R )
        on_btn_czysc_zazn_clicked();
    else if( event->key() == Qt::Key_QuoteLeft || event->key() == Qt::Key_0 )
        ui->combo_tracker->setCurrentIndex(0);
    else if( event->key() == Qt::Key_1 )
        ui->combo_tracker->setCurrentIndex(1);
    else if( event->key() == Qt::Key_2 )
        ui->combo_tracker->setCurrentIndex(2);
    else if( event->key() == Qt::Key_3 )
        ui->combo_tracker->setCurrentIndex(3);
    else if( event->key() == Qt::Key_Z && event->modifiers()==Qt::ControlModifier )
        on_btn_cofnij_clicked();
    else if( event->key() == Qt::Key_Alt )
    {
        ui->video_display->setManipulateOne(true);
        ui->video_display_2->setManipulateOne(true);
    }
    else if( event->key() == Qt::Key_Control )
    {
        ui->video_display->setResizeTool(true);
        ui->video_display_2->setResizeTool(true);
    }
    else if( event->key() == Qt::Key_Shift )
    {
        ui->video_display->setMoveTool(true);
        ui->video_display_2->setMoveTool(true);
    }
}

void MainWindow::keyReleaseEvent( QKeyEvent *event )
{
    if( event->isAutoRepeat() ) return;
    else if( event->key() == Qt::Key_D )
        on_btn_next_released();
    else if( event->key() == Qt::Key_A )
        on_btn_prev_released();
    else if( event->key() == Qt::Key_Alt )
    {
        ui->video_display->setManipulateOne(false);
        ui->video_display_2->setManipulateOne(false);
    }
    else if( event->key() == Qt::Key_Control )
    {
        ui->video_display->setResizeTool(false);
        ui->video_display_2->setResizeTool(false);
    }
    else if( event->key() == Qt::Key_Shift )
    {
        ui->video_display->setMoveTool(false);
        ui->video_display_2->setMoveTool(false);
    }
}

void MainWindow::on_combo_tracker_currentIndexChanged( int index )
{
    if( index > 0 && firstVideo.vstatus.currLabel().tracked ){
        // Set target
        firstVideo.tracker.setTarget( firstVideo.grayImg, firstVideo.vstatus.currLabel().getTargetRect(),
                           index );
        firstVideo.setTrackingContinuity(true);
    }else
        firstVideo.setTrackingContinuity(false);

    ui->video_display->tracking_mode = index;

    if( index > 0 && secondVideo.vstatus.currLabel().tracked ){
        // Set target
        secondVideo.tracker.setTarget( secondVideo.grayImg, secondVideo.vstatus.currLabel().getTargetRect(),
                           index );
        secondVideo.setTrackingContinuity(true);
    }else
        secondVideo.setTrackingContinuity(false);

    ui->video_display_2->tracking_mode = index;
}

void MainWindow::calcGlobalParams(VideoClass &video)
{
    video.vstatus.calcGlobalParams();
    QString label = video.vstatus.all_active ? "TAK" : "NIE";
    ui->lbl_oznaczono_wszystkie->setText( label );
    ui->lbl_max_v->setText( QString::number(video.vstatus.max_v) );
    ui->lbl_max_a->setText( QString::number(video.vstatus.max_a) );
    QString qstr_wymiary = QString::number(video.vstatus.med_w) + "x" + QString::number(video.vstatus.med_h);
    ui->lbl_med_wymiary->setText( qstr_wymiary );
}

void MainWindow::writeToYaml( QString annot_path )
{
    // Write to YAML
    ofstream out_file( annot_path.toStdString().c_str() );
    YAML::Emitter emitter( out_file );
    emitter << YAML::BeginMap;
    yamlEmit( emitter, "rodzaj", ui->combo_rodzaj->currentText().toStdString() );
    yamlEmit( emitter, "widocznosc", ui->combo_widocznosc->currentText().toStdString() );
    yamlEmit( emitter, "szybka_kamera", ui->combo_szybka_kamera->currentText().toStdString() );
    yamlEmit( emitter, "wymijanie_ruch", ui->combo_wymijanie_ruch->currentText().toStdString() );
    yamlEmit( emitter, "wymijanie_stat", ui->combo_wymijanie_stat->currentText().toStdString() );
    yamlEmit( emitter, "l_obiektow", ui->spin_liczba_obiektow->value() );

    yamlEmit( emitter, "first_path", firstVideoPath.toStdString());

    yamlEmit( emitter, "second_path", secondVideoPath.toStdString());

    yamlEmit( emitter, "ozn_wszystkie", firstVideo.vstatus.all_active );
    yamlEmit( emitter, "max_v", firstVideo.vstatus.max_v );
    yamlEmit( emitter, "max_a", firstVideo.vstatus.max_a );
    yamlEmit( emitter, "med_w", firstVideo.vstatus.med_w );
    yamlEmit( emitter, "med_h", firstVideo.vstatus.med_h );

    yamlEmit( emitter, "ozn_wszystkie_2", secondVideo.vstatus.all_active );
    yamlEmit( emitter, "max_v_2", secondVideo.vstatus.max_v );
    yamlEmit( emitter, "max_a_2", secondVideo.vstatus.max_a );
    yamlEmit( emitter, "med_w_2", secondVideo.vstatus.med_w );
    yamlEmit( emitter, "med_h_2", secondVideo.vstatus.med_h );

    emitter << YAML::Key << "labels" << YAML::Value << YAML::BeginSeq;
    for( int i=0; i<firstVideo.vstatus.size(); i++ ){
        emitter << YAML::BeginMap;
        yamlEmit( emitter, "active", firstVideo.vstatus.labels[labelIdx][i].active );
        if( firstVideo.vstatus.labels[labelIdx][i].active ){
            yamlEmit( emitter, "x", firstVideo.vstatus.labels[labelIdx][i].x );
            yamlEmit( emitter, "y", firstVideo.vstatus.labels[labelIdx][i].y );
            yamlEmit( emitter, "w", firstVideo.vstatus.labels[labelIdx][i].w );
            yamlEmit( emitter, "h", firstVideo.vstatus.labels[labelIdx][i].h );
        }
        emitter << YAML::EndMap;
    }
    emitter << YAML::EndSeq;

    emitter << YAML::Key << "labels_2" << YAML::Value << YAML::BeginSeq;
    for( int i=0; i<secondVideo.vstatus.size(); i++ ){
        emitter << YAML::BeginMap;
        yamlEmit( emitter, "active", secondVideo.vstatus.labels[labelIdx][i].active );
        if( secondVideo.vstatus.labels[labelIdx][i].active ){
            yamlEmit( emitter, "x", secondVideo.vstatus.labels[labelIdx][i].x );
            yamlEmit( emitter, "y", secondVideo.vstatus.labels[labelIdx][i].y );
            yamlEmit( emitter, "w", secondVideo.vstatus.labels[labelIdx][i].w );
            yamlEmit( emitter, "h", secondVideo.vstatus.labels[labelIdx][i].h );
        }
        emitter << YAML::EndMap;
    }
    emitter << YAML::EndSeq;

    emitter << YAML::EndMap;

    highlightSave(false);
}

void MainWindow::loadFromYaml( QString annot_path )
{
    // Read annotation
    YAML::Node node = YAML::LoadFile( annot_path.toStdString() );
    setComboByText( ui->combo_rodzaj, node["rodzaj"].as<string>() );
    setComboByText( ui->combo_widocznosc, node["widocznosc"].as<string>() );
    setComboByText( ui->combo_szybka_kamera, node["szybka_kamera"].as<string>() );
    setComboByText( ui->combo_wymijanie_ruch, node["wymijanie_ruch"].as<string>() );
    setComboByText( ui->combo_wymijanie_stat, node["wymijanie_stat"].as<string>() );
    ui->spin_liczba_obiektow->setValue( node["l_obiektow"].as<int>() );

    firstVideoPath = QString::fromStdString( node["first_path"].as<string>() );
    secondVideoPath = QString::fromStdString( node["second_path"].as<string>() );

    openVideo(firstVideoPath.toStdString(), "seq", firstVideo, ui->video_display);
    openVideo(secondVideoPath.toStdString(), "seq", secondVideo, ui->video_display_2);

    for( int i=0; i<node["labels"].size(); i++ ){
        YAML::Node label_node = node["labels"][i];
        Label &label = firstVideo.vstatus.labels[labelIdx][i];
        label.active = label_node["active"].as<bool>();
        if( label.active ){
            label.x = label_node["x"].as<int>();
            label.y = label_node["y"].as<int>();
            label.w = label_node["w"].as<int>();
            label.h = label_node["h"].as<int>();
        }
    }

    for( int i=0; i<node["labels_2"].size(); i++ ){
        YAML::Node label_node = node["labels_2"][i];
        Label &label = secondVideo.vstatus.labels[labelIdx][i];
        label.active = label_node["active"].as<bool>();
        if( label.active ){
            label.x = label_node["x"].as<int>();
            label.y = label_node["y"].as<int>();
            label.w = label_node["w"].as<int>();
            label.h = label_node["h"].as<int>();
        }
    }
    // Update GUI
    calcGlobalParams(firstVideo);
    calcGlobalParams(secondVideo);

    ui->video_bar->draw( firstVideo.vstatus );
    ui->video_display->update( firstVideo.vstatus.currLabel() );
    ui->video_display_2->update( secondVideo.vstatus.currLabel());
}


void MainWindow::on_spin_indeks_celu_valueChanged(int arg1)
{
    labelIdx = arg1-1;
    firstVideo.vstatus.setLabelIndex(arg1-1);
    secondVideo.vstatus.setLabelIndex(arg1-1);
    ui->video_bar->setLabelIndex(arg1-1);
    ui->video_display->update(firstVideo.vstatus.currLabel());
    ui->video_display_2->update(secondVideo.vstatus.currLabel());
}
