#include <video_display.h>

//=====================================================
//======================= UTILS =======================
//=====================================================

//safe mode should be used for tracking, cause it makes them more stable
Point makeValidPoint( const Point &point, const Size &im_sz, bool safe_mode )
{
    Point out_pt = point;
    if( out_pt.x<0 ) out_pt.x = 0;
    if( out_pt.y<0 ) out_pt.y = 0;
    if(safe_mode){
        if( out_pt.x >= im_sz.width-1 ) out_pt.x = im_sz.width-2;
        if( out_pt.y >= im_sz.height-1 ) out_pt.y = im_sz.height-2;
    }
    else{
        if( out_pt.x >= im_sz.width ) out_pt.x = im_sz.width;
        if( out_pt.y >= im_sz.height ) out_pt.y = im_sz.height;
    }
    return out_pt;
}

//=============================================================
//=================== CONSTRUCTOR & DRAWING ===================
//=============================================================

void VideoDisplay::setAllowManipulation(bool value)
{
    allowManipulation = value;
}

void VideoDisplay::setLabelFromRect()
{
    label.setTargetRect(labelRect);
    emit vdisplayEvent(label);
}

VideoDisplay::VideoDisplay( QWidget *parent ) :
    CVImageWidget(parent),
    labelIdx(0),
    tracking_mode(false){}

void VideoDisplay::draw( )
{
    if(draw_prev)
        draw( input, label, prev_label );
    else
        draw( input, label);
}

void VideoDisplay::draw( const Mat &_input, const Label &_label )
{
    if( _input.empty() ) return;
    input = _input;
    label = _label;
    draw_prev = false;
    Mat output = input.clone();

    // Draw label
    if( label.active )
        rectangle( output,
                   Rect(label.x-label.w/2, label.y-label.h/2, label.w, label.h),
                   Scalar(0,0,255) );

    showImage( output );
}

void VideoDisplay::draw(const Mat &_input, const Label &_label, const Label &_prev_label){
    if( _input.empty() ) return;
    input = _input;
    label = _label;
    prev_label = _prev_label;
    draw_prev = true;
    Mat output = input.clone();

    // Draw label
    if( label.active )
        rectangle( output,
                   Rect(label.x-label.w/2, label.y-label.h/2, label.w, label.h),
                   Scalar(0,0,255) );

    //draw previous label
    if(prev_label.active)
        rectangle( output,
                   Rect(prev_label.x-prev_label.w/2, prev_label.y-prev_label.h/2, prev_label.w, prev_label.h),
                   Scalar(150,0,200) );

    showImage( output );
}
void VideoDisplay::drawModified( const Mat &_input )
{
    Mat output = _input.clone();
    draw_prev = false;

    // Draw label
    if( label.active )
        rectangle( output,
                   Rect(label.x-label.w/2, label.y-label.h/2, label.w, label.h),
                   Scalar(0,0,255) );

    showImage( output );
}

void VideoDisplay::update( const Label &_label )
{
    if(draw_prev)
        draw( input, _label, prev_label );
    else
        draw( input, _label);
}

void VideoDisplay::drawBlankFrame( bool bad_frame )
{
    const int side = 50;
    Mat output = Mat::zeros( side, side, CV_8UC3 );
    if( bad_frame ){
        line( output, Point(0,0), Point(side-1,side-1), Scalar(0,0,255) );
        line( output, Point(side-1,0), Point(0,side-1), Scalar(0,0,255) );
    }
    showImage( output );
}


//===============================================================
//======================= WINDOW EVENTS =======================
//===============================================================
void VideoDisplay::resizeEvent(QResizeEvent* event)
{
   QWidget::resizeEvent(event);
   draw(input,label);
}

//============================================================
//======================= MOUSE EVENTS =======================
//============================================================

void VideoDisplay::mousePressEvent( QMouseEvent *event )
{
    if(!allowManipulation) return;
    handleMousePressEvent(event);
    if(!manipulateOne)
        secondDisplay->handleMousePressEvent(event);
}

void VideoDisplay::mouseMoveEvent( QMouseEvent *event )
{
    if(!allowManipulation) return;
    handleMouseMoveEvent(event);
    if(!manipulateOne)
        secondDisplay->handleMouseMoveEvent(event);
}

void VideoDisplay::mouseReleaseEvent( QMouseEvent *event )
{
    if(!allowManipulation) return;
    handleMouseReleaseEvent(event);
    if(!manipulateOne)
        secondDisplay->handleMouseReleaseEvent(event);
}

//=====================================================================
//======================= MOUSE EVENTS HANDLERS =======================
//=====================================================================

void VideoDisplay::handleMousePressEvent( QMouseEvent *event )
{
    // Right btn - clear
    // Keyboard will transform selection into tracker or go tracker mode

    if( input.empty() ) return;
    if( event->button() == Qt::LeftButton ){
        Point wgt_pt( event->localPos().x(), event->localPos().y() );
        Point img_pt = widget2ImageCoords( wgt_pt );
        down_pt = img_pt;
        mouse_down = true;
        if(isMoveTool && !isResizeTool){
            moveLabel( event->localPos().x(), event->localPos().y() );
        }
        if(isResizeTool && !isMoveTool) {
            resizeLabel( event ->localPos().x(), event->localPos().y() );
        }
    }
}

void VideoDisplay::handleMouseMoveEvent( QMouseEvent *event )
{
    if( input.empty() ) return;
    if( mouse_down ){
        if(isMoveTool && !isResizeTool){
            moveLabel( event->localPos().x(), event->localPos().y() );
        }
        if(isResizeTool && !isMoveTool) {
            resizeLabel( event ->localPos().x(), event->localPos().y() );
        }
        if (!isMoveTool && !isResizeTool){
            makeNewLabel( event->localPos().x(), event->localPos().y() );
        }
    }
}

void VideoDisplay::handleMouseReleaseEvent( QMouseEvent *event )
{
    if(event->button() == Qt::LeftButton){
        mouse_down = false;
        emit vdisplayEvent(label);
    }
}


//==================================================================
//======================= LABEL MANIPULATION =======================
//==================================================================
void VideoDisplay::makeNewLabel(int x_, int y_){
    Point wgt_pt( x_, y_ );
    Point img_pt = widget2ImageCoords( wgt_pt );
    Label new_label = label;

    Point A = makeValidPoint( down_pt, input.size(), tracking_mode );
    Point B = makeValidPoint( img_pt, input.size(), tracking_mode );

    int x = 0.5*(A.x+B.x);
    int y = 0.5*(A.y+B.y);
    int w = abs(A.x-B.x);
    int h = abs(A.y-B.y);
    if(w*h <= 9) return;

    new_label.x = x; new_label.y = y;
    new_label.w = w; new_label.h = h;
    new_label.active = true;
    if( tracking_mode ) {
        new_label.tracked = true;
    }

    label = new_label;
    label.index = labelIdx;
    if(draw_prev)
        draw(input, label, prev_label);
    else
        draw( input, label );
}

void VideoDisplay::moveLabel(int x_, int y_){
    Point wgt_pt( x_, y_ );
    Point img_pt = widget2ImageCoords( wgt_pt );
    int xLabel, yLabel, wLabel, hLabel;
    xLabel = label.x;
    yLabel = label.y;
    wLabel = label.w;
    hLabel = label.h;

    if (xLabel < 0 || yLabel < 0 || wLabel < 0 || hLabel < 0) return;

    int xDistance = img_pt.x - xLabel;
    int yDistance = img_pt.y - yLabel;

    Point A, B;
    A.x = xLabel - wLabel/2  + xDistance;
    A.y = yLabel - hLabel/2 + yDistance;
    B.x = xLabel + wLabel/2 + xDistance;
    B.y = yLabel + hLabel/2 + yDistance;
    A = makeValidPoint( A, input.size(), tracking_mode );
    B = makeValidPoint( B, input.size(), tracking_mode );

    int newX = 0.5*(A.x+B.x);
    int newY = 0.5*(A.y+B.y);

    label.x = newX; label.y = newY;
    label.active = true;
    if( tracking_mode ){
        label.tracked = true;
    }

    if(draw_prev)
        draw(input, label, prev_label);
    else
        draw( input, label );
}

void VideoDisplay::resizeLabel(int x_, int y_)
{
    Point wgt_pt( x_, y_ );
    Point img_pt = widget2ImageCoords( wgt_pt );
    int xLabel, yLabel, wLabel, hLabel;
    xLabel = label.x;
    yLabel = label.y;
    wLabel = label.w;
    hLabel = label.h;

    if (xLabel < 0 || yLabel < 0 || wLabel < 0 || hLabel < 0) return;

    int xDistance = 2 * ( img_pt.x - xLabel );
    int yDistance = 2 * ( img_pt.y - yLabel );
    if ( xDistance < 10 || yDistance < 10 ) return;

    label.w = xDistance; label.h = yDistance;
    label.active = true;
    if( tracking_mode ){
        label.tracked = true;
    }

    if(draw_prev)
        draw(input, label, prev_label);
    else
        draw( input, label );
}

void VideoDisplay::setSeg(Segmentation *value)
{
    seg = value;
}

//==================================================================
//======================= SEGMENTATION =============================
//==================================================================
void VideoDisplay::segmentGrabCut()
{
    if ( !seg ) return;
    if( input.empty() || !( label.active || label.tracked ) ) return;
    cv::Mat result = seg->grabCutSegment( input, label );
    labelRect = seg->getGrabCutRect();
    drawModified( result );
}

void VideoDisplay::segmentMeanShift()
{
    if ( !seg ) return;
    if( input.empty() ) return;
    cv::Mat result = seg->meanShiftSegment( input );
    drawModified( result );
}

void VideoDisplay::segmentWatershed()
{
    if ( !seg ) return;
    //if( input.empty() || !( label.active || label.tracked )) return;
    if( input.empty()) return;
    cv::Mat result = seg->watershedSegment( input );
    drawModified( result );
}
