#include <cv_image_widget.h>

CVImageWidget::CVImageWidget( QWidget *parent ) :
    QWidget(parent), x_margin(0), y_margin(0), x_factor(1), y_factor(1){}

QSize CVImageWidget::sizeHint() const { return _qimage.size(); }

QSize CVImageWidget::minimumSizeHint() const { return _qimage.size(); }

void CVImageWidget::showImage( const cv::Mat& image, bool fixed_shape )
{
    // Resize the image
    cv::Mat canvas;
    int widget_height = this->height();
    int widget_width = this->width();
    cv::Size new_sz( widget_width, widget_height );
    // Add black margins to mantain image proportions
    if( fixed_shape ){
        float im_ratio = float(image.cols)/image.rows;
        float widget_ratio = float(widget_width)/widget_height;
        if( im_ratio < widget_ratio ){
            new_sz.height = widget_height;
            new_sz.width = image.cols * widget_height / image.rows;
        }else{
            new_sz.width = widget_width;
            new_sz.height = image.rows * widget_width / image.cols;
        }
        canvas = cv::Mat::zeros( widget_height, widget_width, CV_8UC3 );
        x_margin = (widget_width-new_sz.width)/2;
        y_margin = (widget_height-new_sz.height)/2;
        cv::Mat roi = canvas( cv::Rect( x_margin, y_margin,
                                        new_sz.width, new_sz.height ) );
        cv::resize( image, roi, new_sz );
    // Stretch the image
    }else{
        cv::resize( image, canvas, new_sz );
        x_margin = 0;
        y_margin = 0;
    }
    x_factor = double(new_sz.width) / image.cols;
    y_factor = double(new_sz.height) / image.rows;

    // Convert the image to the RGB888 format
    switch (image.type()) {
    case CV_8UC1:
        cvtColor( canvas, _input_image, CV_GRAY2RGB );
        break;
    case CV_8UC3:
        cvtColor( canvas, _input_image, CV_BGR2RGB );
        break;
    }

    repaint();
}

void CVImageWidget::paintEvent( QPaintEvent* /*event*/ )
{
    if(_input_image.empty()) return;
    _input_image.copyTo(_tmp);

    // QImage needs the data to be stored continuously in memory
    //assert(_tmp.isContinuous());
    if( !_tmp.isContinuous() ) return;
    // Assign OpenCV's image buffer to the QImage. Note that the bytesPerLine parameter
    // (http://qt-project.org/doc/qt-4.8/qimage.html#QImage-6) is 3*width because each pixel
    // has three bytes.
    _qimage = QImage(_tmp.data, _tmp.cols, _tmp.rows, _tmp.cols*3, QImage::Format_RGB888);

    // Display the image
    QPainter painter(this);
    painter.drawImage(QPoint(0,0), _qimage);
    painter.end();
}

cv::Point CVImageWidget::widget2ImageCoords( const Point &widget_pt )
{
    return Point( (widget_pt.x-x_margin)/x_factor, (widget_pt.y-y_margin)/y_factor );
}
