#include "sequencedialog.h"
#include "ui_sequencedialog.h"

SequenceDialog::SequenceDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SequenceDialog)
{
    ui->setupUi(this);
    setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);
}

SequenceDialog::~SequenceDialog()
{
    delete ui;
}

void SequenceDialog::checkIfReady()
{
    if(photosOk && nameOk && dirOk)
        ui->generateSequenceButton->setEnabled(true);
    else
        ui->generateSequenceButton->setEnabled(false);
}

void SequenceDialog::checkFileName()
{
    bool ret = true;
    QString errorCode = "";
    for(int i = 0; i < sequenceName.size() - 1; i++)
    {
        if(sequenceName.at(i) == "_")
        {
            errorCode += "Bez podkreślnika!";
            ret = false;
        }
        if(sequenceName.at(i) == ' ')
        {
            errorCode += "Bez spacji! ";
            ret = false;
        }
    }
    if(!ret) ui->fileNameLabel->setText(errorCode);
    else ui->fileNameLabel->setText("Poprawna nazwa!");
    nameOk = ret;
    checkIfReady();
}

//===============================================================
//======================= INTERFACE SLOTS =======================
//===============================================================

void SequenceDialog::on_photosButton_clicked()
{
    selectedPhotoPaths = QFileDialog::getOpenFileNames(this,
        tr("Wybierz pliki obrazkowe"), QDir::homePath(), tr("Pliki obrazkowe (*.png *.jpg *.bmp)"));
    if(selectedPhotoPaths.size() > 0)
    {
        ui->photosLabel->setText("Wybrano " + QString::number(selectedPhotoPaths.size()) + " obrazów.");
        photosOk = true;
    }
    checkIfReady();
}

void SequenceDialog::on_sequencNameEdit_textChanged(const QString &arg1)
{
    sequenceName = arg1 + "_";
    checkFileName();
}

void SequenceDialog::on_sequenceDirButton_clicked()
{
    sequenceDir = QFileDialog::getExistingDirectory(this, tr("Wybierz folder"),
                                                 sequenceDir,
                                                 QFileDialog::ShowDirsOnly
                                                 | QFileDialog::DontResolveSymlinks);

    if(QDir(sequenceDir).entryInfoList(QDir::NoDotAndDotDot|QDir::AllEntries).count() == 0)
    {
        ui->sequenceDirLabel->setText("Poprawny folder!");
        ui->sequenceDirEdit->setText(sequenceDir);
        dirOk = true;
    }
    else
    {
        ui->sequenceDirLabel->setText("Folder musi być pusty!");
    }
    checkIfReady();
}

void SequenceDialog::on_sequenceDirEdit_textChanged(const QString &arg1)
{
    if(QDir(arg1).entryInfoList(QDir::NoDotAndDotDot|QDir::AllEntries).count() == 0)
    {
        sequenceDir = arg1;
        dirOk = true;
    }
    checkIfReady();
}

void SequenceDialog::on_generateSequenceButton_clicked()
{
    thread = new GenerateSequenceThread(selectedPhotoPaths, sequenceDir, sequenceName);
    QObject::connect(thread, SIGNAL(setProgress(int,int)),  this, SLOT(on_generateSequenceThread_progress(int,int)));
    QObject::connect(thread, SIGNAL(finished()),            this, SLOT(on_generateSequenceThread_finished()));
    thread->run();
}

void SequenceDialog::on_getSequenceButton_clicked()
{
    sequencePath = QFileDialog::getOpenFileName(this,
        tr("Wybierz pierwsze zdjęcie sekwencji"), sequencePath_2, tr("Plik obrazkowe (*.png *.jpg *.bmp)"));

    QFileInfo fi(sequencePath);
    QString fileName = fi.fileName();
    sequencePath.remove(fileName);
    QString sequenceExtention;
    sequenceExtention = fileName.mid(fileName.size() - 3, fileName.size());
    int digits = 0;
    QString digitsString = fileName.section('_',1,1);
    digits = digitsString.size() - 4;
    sequenceName = fileName.section('_',0,0);

    sequencePath += sequenceName + "_%0" + QString::number(digits)+"d."+sequenceExtention;
    ui->readySequenceLabel->setText("Ścieżka sekwencji: "+sequencePath);
    pathValid = true;
}

void SequenceDialog::on_getSequenceButton_3_clicked()
{
    sequencePath_2 = QFileDialog::getOpenFileName(this,
        tr("Wybierz pierwsze zdjęcie sekwencji"), sequencePath, tr("Plik obrazkowe (*.png *.jpg *.bmp)"));

    QFileInfo fi(sequencePath_2);
    QString fileName = fi.fileName();
    sequencePath_2.remove(fileName);
    QString sequenceExtention;
    sequenceExtention = fileName.mid(fileName.size() - 3, fileName.size());
    int digits = 0;
    QString digitsString = fileName.section('_',1,1);
    digits = digitsString.size() - 4;
    sequenceName_2 = fileName.section('_',0,0);

    sequencePath_2 += sequenceName_2 + "_%0" + QString::number(digits)+"d."+sequenceExtention;
    ui->readySequenceLabel_2->setText("Ścieżka sekwencji: "+sequencePath_2);
    pathValid_2 = true;
}

void SequenceDialog::on_getSequenceButton_4_clicked()
{
    sequencePath_3 = QFileDialog::getOpenFileName(this,
        tr("Wybierz pierwsze zdjęcie sekwencji"), sequencePath, tr("Plik obrazkowe (*.png *.jpg *.bmp)"));

    QFileInfo fi(sequencePath_3);
    QString fileName = fi.fileName();
    sequencePath_3.remove(fileName);
    QString sequenceExtention;
    sequenceExtention = fileName.mid(fileName.size() - 3, fileName.size());
    int digits = 0;
    QString digitsString = fileName.section('_',1,1);
    digits = digitsString.size() - 4;
    sequenceName_3 = fileName.section('_',0,0);

    sequencePath_3 += sequenceName_3 + "_%0" + QString::number(digits)+"d."+sequenceExtention;
    ui->readySequenceLabel_3->setText("Ścieżka sekwencji: "+sequencePath_2);
    pathValid_3 = true;
}

void SequenceDialog::on_buttonBox_accepted()
{
    if(pathValid || pathValid_2)
        emit sequencePathReady(sequencePath, sequenceName, sequencePath_2, sequenceName_2, sequencePath_3, sequenceName_3);
}

void SequenceDialog::on_generateSequenceThread_progress(int file, int files)
{
    if(file != files)
    {
        ui->generateSequenceLabel->setText("Generuję... "+QString::number(file)+"/" + QString::number(files));
    }
    else
    {
        ui->generateSequenceLabel->setText("Wygenerowano "+QString::number(files)+" plików sekwencji.");
    }
    QCoreApplication::processEvents();
}

void SequenceDialog::on_generateSequenceThread_finished()
{
    delete thread;
}


