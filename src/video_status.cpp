#include <video_status.h>

int NUM_OF_LABELS = 10;

//=====================================================
//======================= LABEL =======================
//=====================================================

Label::Label() : active(false), tracked(false){}

Rect Label::getTargetRect() const
{
    return Rect(x-w/2, y-h/2, w, h);
}

void Label::setTargetRect( const Rect &target )
{
    x = target.x+target.width/2;
    y = target.y+target.height/2;
    w = target.width;
    h = target.height;
    if( w*h > 9 ) {
        active = true;
        tracked = true;
    }
}

Rect Label::getActiveRect() const
{
    return Rect(x-w/2, y-h/2, w, h);
}

//=================================================================
//======================= VIDEO STATUS MISC =======================
//=================================================================

VideoStatus::VideoStatus() :
    all_active(false), max_v(0), max_a(0), med_w(0), med_h(0), label_index(0)
{
    reset();
}

void VideoStatus::init( int num_frames )
{
    labels.clear();
    labels.resize(NUM_OF_LABELS);
    for(size_t i = 0; i < NUM_OF_LABELS; i++)
    {
        labels[i].resize(num_frames);
        curr = 0;
        sel_start = -1;
        sel_end = -1;
    }
}

int VideoStatus::size() const
{
    return labels[label_index].size();
}

Label& VideoStatus::currLabel()
{
    if( size() > 0 ) return labels[label_index][curr];
    else return default_label;
}

Label& VideoStatus::prevLabel()
{
    if( size() > 0 && curr > 0) return labels[label_index][curr-1];
    else return default_label;
}

void VideoStatus::reset()
{
    init(0);
}

//=========================================================
//======================= SELECTION =======================
//=========================================================

int VideoStatus::getSelStart() const
{
    return sel_start;
}

int VideoStatus::getSelEnd() const
{
    return sel_end;
}

void VideoStatus::setSelStart()
{
    if( size() == 0 ) return;
    sel_start = curr;
    if( sel_end < sel_start )
        sel_end = sel_start;
}

void VideoStatus::setSelEnd()
{
    if( size() == 0 ) return;
    sel_end = curr;
    if( sel_start > sel_end || sel_start < 0 )
        sel_start = sel_end;
}

void VideoStatus::selectBehind()
{
    if( size() == 0 ) return;
    sel_end = curr;
    if( sel_end==0 ) sel_start = sel_end;
    else for( sel_start=sel_end-1; sel_start>0; sel_start-- )
        if( labels[label_index][sel_start].active ) break;
}

void VideoStatus::clearSelection()
{
    sel_end = -1;
    sel_start = -1;
}

//========================================================
//=================== TARGET & TRACKER ===================
//========================================================

void VideoStatus::clearTarget()
{
    if( size() == 0 ) return;
    if( sel_start < 0 )
        labels[label_index][curr] = Label();
    else for( int i=sel_start; i<=sel_end; i++ ){
        labels[label_index][i] = Label();
    }
}

void VideoStatus::clearTracker()
{
    if( size() == 0 ) return;
    if( sel_start < 0 )
        labels[label_index][curr].tracked = false;
    else for( int i=sel_start; i<=sel_end; i++ ){
        labels[label_index][i].tracked = false;
    }
}

void VideoStatus::interpolatePosition()
{
    // Check valid selection
    if( sel_end - sel_start < 2 ||
        !labels[label_index][sel_start].active ||
        !labels[label_index][sel_end].active ) return;

    // Calculate params
    int sel_count = sel_end-sel_start;
    double delta_x = double(labels[label_index][sel_end].x - labels[label_index][sel_start].x) / sel_count;
    double delta_y = double(labels[label_index][sel_end].y - labels[label_index][sel_start].y) / sel_count;
    for( int i=1; i<sel_count; i++ ){

        // Position interpolation
        labels[label_index][sel_start+i].x = labels[label_index][sel_start].x + i*delta_x;
        labels[label_index][sel_start+i].y = labels[label_index][sel_start].y + i*delta_y;

        // If unactive, copy size from previous
        if( !labels[label_index][sel_start+i].active ){
            labels[label_index][sel_start+i].active = true;
            labels[label_index][sel_start+i].w = labels[label_index][sel_start+i-1].w;
            labels[label_index][sel_start+i].h = labels[label_index][sel_start+i-1].h;
        }
    }
}

void VideoStatus::interpolateSize()
{
    // Check valid selection
    if( sel_end - sel_start < 2 ||
        !labels[label_index][sel_start].active ||
        !labels[label_index][sel_end].active ) return;

    // Calculate params
    int sel_count = sel_end-sel_start;
    double delta_w = double(labels[label_index][sel_end].w - labels[label_index][sel_start].w) / sel_count;
    double delta_h = double(labels[label_index][sel_end].h - labels[label_index][sel_start].h) / sel_count;
    for( int i=1; i<sel_count; i++ ){

        // Size interpolation
        labels[label_index][sel_start+i].w = labels[label_index][sel_start].w + i*delta_w;
        labels[label_index][sel_start+i].h = labels[label_index][sel_start].h + i*delta_h;

        // If unactive, copy position from previous
        if( !labels[label_index][sel_start+i].active ){
            labels[label_index][sel_start+i].active = true;
            labels[label_index][sel_start+i].x = labels[label_index][sel_start+i-1].x;
            labels[label_index][sel_start+i].y = labels[label_index][sel_start+i-1].y;
        }
    }
}

//=============================================================
//======================= GLOBAL PARAMS =======================
//=============================================================

typedef pair<int,int> indexedArea;
bool areaCompare( const indexedArea& l, const indexedArea& r)
{ return l.second < r.second; }

void VideoStatus::calcGlobalParams()
{
    // Init variables
    max_v = max_a = 0;
    med_w = med_h = 0;
    all_active = false;
    vector<indexedArea> areas;
    areas.reserve( size() );
    if( size() == 0 ) return;

    all_active = true;
    for( int i=0; i<size(); i++ ){
        // All active
        if( !labels[label_index][i].active ){
            all_active = false;
            continue;
        }

        // Area
        areas.push_back( indexedArea(i, labels[label_index][i].w*labels[label_index][i].h) );

        // Speed - v
        if( i<1 ) continue;
        if( !labels[label_index][i-1].active ) continue;
        Point trans1( labels[label_index][i].x-labels[label_index][i-1].x, labels[label_index][i].y-labels[label_index][i-1].y );
        float v1 = sqrt( trans1.x*trans1.x + trans1.y*trans1.y );
        if( v1 > max_v ) max_v = v1;

        // Acceleration - a
        if( i<2 ) continue;
        if( !labels[label_index][i-2].active ) continue;
        Point trans2( labels[label_index][i-1].x-labels[label_index][i-2].x, labels[label_index][i-1].y-labels[label_index][i-2].y );
        float v2 = sqrt( trans2.x*trans2.x + trans2.y*trans2.y );
        float a = v1-v2;
        if( a > max_a ) max_a = a;
    }

    // Median size
    if( areas.size() > 0 ){
        sort( areas.begin(), areas.end(), areaCompare );
        med_w = labels[label_index][ areas[areas.size()/2].first ].w;
        med_h = labels[label_index][ areas[areas.size()/2].first ].h;
    }
}
