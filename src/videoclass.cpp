#include "videoclass.h"

bool VideoClass::getTrackingContinuity() const
{
    return trackingContinuity;
}

string VideoClass::getVideoName() const
{
    return videoName;
}

string VideoClass::getVideoDir() const
{
    return videoDir;
}

void VideoClass::setTrackingContinuity(bool value)
{
    trackingContinuity = value;
}

void VideoClass::setVideoName(const string &value)
{
    videoName = value;
}

void VideoClass::setVideoDir(const string &value)
{
    videoDir = value;
}

VideoClass::VideoClass() :
    trackingContinuity(false)
{
    
}

