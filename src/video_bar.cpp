#include <video_bar.h>

VideoBar::VideoBar( QWidget *parent ) :
    CVImageWidget(parent), mouse_down(false), label_index(0) {}

//=======================================================
//======================= DRAWING =======================
//=======================================================

void VideoBar::draw( const VideoStatus &vstatus )
{
    // Params
    Scalar pos_color(255,255,0);
    Scalar selection_color(0,150,150);
    Scalar target_frame_color(0,0,150);

    // Black if there are no frames
    if( vstatus.size() == 0 ){
        clear();
        return;
    }

    // Widget dimensions
    int widget_height = this->geometry().height();
    int widget_w = this->geometry().width();
    double frame_w = double(widget_w) / vstatus.size();
    int drawing_w = ceil(frame_w);

    // Draw
    Mat output = Mat::zeros( widget_height, widget_w, CV_8UC3 );

    // Draw frames
    for( int i=0; i<vstatus.size(); i++ ){
        if( !vstatus.labels[label_index][i].active ) continue;
        int x = i*frame_w;
        rectangle( output, Rect(x, widget_height/2, drawing_w, widget_height/2),
                   target_frame_color, -1 );
    }

    // Draw selection
    if( vstatus.getSelStart() >= 0 ){
        int x = vstatus.getSelStart()*frame_w;
        int w = (vstatus.getSelEnd()-vstatus.getSelStart()+1)*frame_w;
        rectangle( output, Rect(x, 0, w, widget_height/2),
                   selection_color, -1 );
    }

    // Draw current pos marker
    int poly_x = (vstatus.curr+0.5) * frame_w;
    int poly_y = widget_height/2;
    int poly_w = widget_height/2;
    int poly_h = poly_w;
    vector<Point> poly(4);
    poly[0] = Point( poly_x-poly_w/2, poly_y );
    poly[1] = Point( poly_x, poly_y-poly_h/2 );
    poly[2] = Point( poly_x+poly_w/2, poly_y );
    poly[3] = Point( poly_x, poly_y+poly_h/2 );
    fillConvexPoly( output, poly, pos_color );

    showImage( output );
}

void VideoBar::clear()
{
    Mat black = Mat::zeros( 100, 100, CV_8UC3 );
    black.setTo( Scalar::all(0) );
    showImage( black );
}

//============================================================
//======================= MOUSE EVENTS =======================
//============================================================

void VideoBar::mousePressEvent( QMouseEvent *event )
{
    if( event->button() == Qt::LeftButton ){
        mouse_down = true;
        emit vbarEvent( event->localPos().x() );
    }
}

void VideoBar::mouseMoveEvent( QMouseEvent *event )
{
    if( mouse_down )
        emit vbarEvent( event->localPos().x() );
}

void VideoBar::mouseReleaseEvent( QMouseEvent *event )
{
    if(event->button() == Qt::LeftButton){
        mouse_down = false;
    }
}
