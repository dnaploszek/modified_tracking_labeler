#include "segmentation.h"

#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/gpu/gpu.hpp"

Segmentation::Segmentation()
{
    gcIterations = 1;

    spatialWindow = 10;
    colorWindow = 2;
    maxPyr = 1;
}

void Segmentation::setSegmentationOption(ValueCode code, int val)
{
    switch(code)
    {
    case ValueCode::GC_ITER:
        gcIterations = val;
        break;
    case ValueCode::MS_SPATIAL:
        spatialWindow = val;
        break;
    case ValueCode::MS_COLOR:
        colorWindow = val;
        break;
    case ValueCode::MS_MAXPYR:
        maxPyr = val;
        break;
    }
}

cv::Mat Segmentation::grabCutSegment(cv::Mat img, Label label)
{
    cv::Mat result;
    cv::Mat bgModel,fgModel;
    cv::grabCut(img, result, label.getActiveRect(), bgModel, fgModel, gcIterations, cv::GC_INIT_WITH_RECT);
    cv::compare(result, cv::GC_PR_FGD, result, cv::CMP_EQ);
    cv::Mat points;
    cv::findNonZero(result,points);
    gcBoundingRect = cv::boundingRect(points);
    // Generate output image
    cv::Mat foreground(img.size(),CV_8UC4,cv::Scalar(255,255,255,0));
    img.copyTo(foreground,result);

    return foreground;
}

cv::Mat Segmentation::meanShiftSegment(cv::Mat img)
{
    /*
     * Parameters:
        src – The source 8-bit, 3-channel image.
        dst – The destination image of the same format and the same size as the source.
        sp – The spatial window radius.
        sr – The color window radius.
        maxLevel – Maximum level of the pyramid for the segmentation.
        termcrit – Termination criteria: when to stop meanshift iterations.
     */
    cv::Mat result;
    pyrMeanShiftFiltering( img, result, spatialWindow, colorWindow, maxPyr );

    cv::RNG rng = cv::theRNG();
    cv::Scalar colorDiff = cv::Scalar::all(1);
    cv::Mat mask( result.rows+2, result.cols+2, CV_8UC1, cv::Scalar::all(0) );
    for( int y = 0; y < result.rows; y++ )
    {
        for( int x = 0; x < result.cols; x++ )
        {
            if( mask.at<uchar>(y+1, x+1) == 0 )
            {
                cv::Scalar newVal( rng(256), rng(256), rng(256) );
                cv::floodFill( result, mask, Point(x,y), newVal, 0, colorDiff, colorDiff );
            }
        }
    }
    return result;
}

cv::Mat Segmentation::watershedSegment(cv::Mat img)
{
    cv::Mat result;
    // Create a kernel that we will use for accuting/sharpening our image
        Mat kernel = (Mat_<float>(3,3) <<
                1,  1, 1,
                1, -8, 1,
                1,  1, 1);

        Mat imgLaplacian;
        Mat sharp = img; // copy source image to another temporary one
        filter2D(sharp, imgLaplacian, CV_32F, kernel);
        img.convertTo(sharp, CV_32F);
        result = sharp - imgLaplacian;
        // convert back to 8bits gray scale
        result.convertTo(result, CV_8UC3);
        imgLaplacian.convertTo(imgLaplacian, CV_8UC3);


        // Create binary image from source image
        Mat bw;
        cvtColor(img, bw, CV_BGR2GRAY);
        threshold(bw, bw, 40, 255, CV_THRESH_BINARY | CV_THRESH_OTSU);

        // Perform the distance transform algorithm
        Mat dist;
        distanceTransform(bw, dist, CV_DIST_L2, 3);
        // Normalize the distance image for range = {0.0, 1.0}
        // so we can visualize and threshold it
        normalize(dist, dist, 0, 1., NORM_MINMAX);

        // Threshold to obtain the peaks
        // This will be the markers for the foreground objects
        threshold(dist, dist, .4, 1., CV_THRESH_BINARY);
        // Dilate a bit the dist image
        Mat kernel1 = Mat::ones(3, 3, CV_8UC1);
        dilate(dist, dist, kernel1);

        // Create the CV_8U version of the distance image
        // It is needed for findContours()
        Mat dist_8u;
        dist.convertTo(dist_8u, CV_8U);
        // Find total markers
        vector<vector<Point> > contours;
        findContours(dist_8u, contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);
        // Create the marker image for the watershed algorithm
        Mat markers = Mat::zeros(dist.size(), CV_32SC1);
        // Draw the foreground markers
        for (size_t i = 0; i < contours.size(); i++)
            drawContours(markers, contours, static_cast<int>(i), Scalar::all(static_cast<int>(i)+1), -1);
        // Draw the background marker
        circle(markers, Point(5,5), 3, CV_RGB(255,255,255), -1);

        // Perform the watershed algorithm
        watershed(img, markers);
        Mat mark = Mat::zeros(markers.size(), CV_8UC1);
        markers.convertTo(mark, CV_8UC1);
        bitwise_not(mark, mark);
        //    imshow("Markers_v2", mark); // uncomment this if you want to see how the mark
                                          // image looks like at that point
        // Generate random colors
        vector<Vec3b> colors;
        for (size_t i = 0; i < contours.size(); i++)
        {
            int b = theRNG().uniform(0, 255);
            int g = theRNG().uniform(0, 255);
            int r = theRNG().uniform(0, 255);
            colors.push_back(Vec3b((uchar)b, (uchar)g, (uchar)r));
        }
        // Create the result image
        Mat dst = Mat::zeros(markers.size(), CV_8UC3);
        // Fill labeled objects with random colors
        for (int i = 0; i < markers.rows; i++)
        {
            for (int j = 0; j < markers.cols; j++)
            {
                int index = markers.at<int>(i,j);
                if (index > 0 && index <= static_cast<int>(contours.size()))
                    dst.at<Vec3b>(i,j) = colors[index-1];
                else
                    dst.at<Vec3b>(i,j) = Vec3b(0,0,0);
            }
        }
        // Visualize the final image

        return dst;
}

Rect Segmentation::getGrabCutRect()
{
    return gcBoundingRect;
}

