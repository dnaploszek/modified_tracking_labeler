#include "tracker.h"

const int CENTROID_WHITE=1;
const int CENTROID_DARK=2;
const int TEMPLATE=3;

//=====================================================
//======================= UTILS =======================
//=====================================================

/** \brief Add margins to a rect */
Rect getWiderRect( const Rect &in_rect, const Size &im_sz,
                   int x_margin, int y_margin=-1 )
{
    if( y_margin < 0 ) y_margin = x_margin;
    Rect out_rect( in_rect.x-x_margin, in_rect.y-y_margin,
                   in_rect.width+2*x_margin, in_rect.height+2*y_margin );

    out_rect.width = min(im_sz.width, out_rect.width);
    out_rect.height = min(im_sz.height, out_rect.height);

    out_rect.x = max( 0, out_rect.x );
    out_rect.y = max( 0, out_rect.y );
    out_rect.x = min( im_sz.width-out_rect.width, out_rect.x );
    out_rect.y = min( im_sz.height-out_rect.height, out_rect.y );
    return out_rect;
}

//======================================================
//======================= TACKER =======================
//======================================================

Tracker::Tracker() :
    templ_mx(20),
    templ_my(20),
    mode( CENTROID_WHITE ),
    centroid_criteria( TermCriteria( CV_TERMCRIT_ITER, 1, 0 ) ) {} // 1 iteration

void Tracker::setTarget( const Mat &image, const Rect& rect, int _mode )
{
    // Initialize if needed
    bool init = false;
    if( prepr_img.empty() ) init = true;
    else if( prepr_img.size() != image.size() ) init = true;
    if( init ){
        prepr_img = Mat::zeros( image.size(), CV_8UC1 );
        tmp = Mat( image.size(), CV_8UC1 );
    }

    // Set the target and tracker mode
    mode = _mode;
    curr_target = getWiderRect( rect, image.size(), 0 );
    if( mode == TEMPLATE ){
        templ = image(rect).clone();
    }
}

Rect Tracker::track( Mat &image )
{
    preprocess( image, prepr_img );
    //imshow( "Preproc", prepr_img );
    //imshow( "Preproc CPU", prepr_img );
    //waitKey(0);

    if( mode == CENTROID_WHITE || mode == CENTROID_DARK ){
        meanShift( prepr_img, curr_target, centroid_criteria );
    }else if( mode == TEMPLATE ){
        Rect search_rect = getWiderRect( curr_target, image.size(), templ_mx, templ_my );
        Mat roi = prepr_img( search_rect );
        matchTemplate( roi, templ, match_res, CV_TM_CCOEFF_NORMED );
        int max_idx[2];
        minMaxIdx( match_res, 0, 0, 0, max_idx );
        curr_target.x = search_rect.x + max_idx[1];
        curr_target.y = search_rect.y + max_idx[0];
    }
    return curr_target;
}

void Tracker::preprocess( const Mat &input, Mat &output )
{
    // ROI
    Mat in_roi, out_roi;
    Rect roi_rect;
    if( mode == CENTROID_WHITE || mode == CENTROID_DARK ){
        roi_rect = curr_target;
    }else if( mode == TEMPLATE ){
        roi_rect = getWiderRect( curr_target, input.size(), templ_mx, templ_my );
    }
    in_roi = input(roi_rect);
    out_roi = output(roi_rect);

    // Filtering
    if( mode == TEMPLATE ){
        in_roi.copyTo( out_roi );
    }else if( mode == CENTROID_WHITE ){
        adaptiveThreshold(in_roi, out_roi, 255, ADAPTIVE_THRESH_MEAN_C, THRESH_BINARY, 11, -10 );
    }else if( mode == CENTROID_DARK ){
        adaptiveThreshold( in_roi, out_roi, 255, ADAPTIVE_THRESH_MEAN_C, THRESH_BINARY_INV, 11, 10 );
    }
}
